function img_segm = km_segm(img, radius, alpha, kernel, lpf,Img22,thss)
% This Matlab function is part of the code package released as a
% supplementary material for the article: 
%
% "Evaluation of methods for detection of fluorescence labeled 
% subcellular objects in microscope images" by P. Ruusuvuori et al. 
%
% We kindly request you to acknowledge the authors properly 
% (citation or request for permission from the authors) when using this
% function.
%
% Website: http://www.cs.tut.fi/sgn/csb/subcell
%
%
%KM_SEGM    Kernel method based segmentation algorithm
%
%   usage: img_segm = km_segm(img, radius, alpha, kernel)
%
%   where:
%       img         3D or 2D array which contains the original image
%       radius      radius of the mask (circle)
%       alpha       scaling constant
%       kernel      possible values are: uniform, triangle, epanechnikov,
%                   quartic, triweight, gaussian, cosine
%       img_segm    3D or 2D array which contains the image after the
%                   segmentation
%   
%   See also http://en.wikipedia.org/wiki/Kernel_(statistics)
%
%   Please cite article "Ruusuvuori et al.: Evaluation of methods for
%   detection of fluorescence labeled subcellular objects in microscope
%   images" when using this function.
%
%   Author: Tarmo �ij� <tarmo.aijo@tut.fi>

% image should be 2D or 3D

if (length(size(img)) ~= 2 && length(size(img)) ~= 3) || nargin == 0
    error '2D or 3D image is required.'
end

img = double(img);

% default radius is 5
if nargin < 2
    radius = 5;
end

% default alpha is 0.1
if nargin < 3
    alpha = 0.1;
end

% default kernel is uniform
if nargin < 4
    kernel = 'uniform';
end
if nargin < 5
    % perform low-pass filtering 1 = yes, 0 = no
    lpf = 0;
end
% low-pass filtering
if lpf == 1
    lpfl = 3;
    img = padarray(img,[lpfl lpfl],'both','replicate');
    img = filter2(ones(lpfl)/lpfl^2,img,'same');
    img = img(lpfl+1:end-lpfl,lpfl+1:end-lpfl);
end

% scale image values to interval [0,1]
img = img-min(img(:));
img = img./max(img(:));

% add zeros to borders 
pad_img = padarray(img, [radius radius]);

% kerneled image
img_kernel = zeros(size(img));

% mask is circle
mask = fspecial('disk', radius);
%mask(radius+1,radius+1) = 0;
indices = find(mask > 0);
n = length(indices);

% filter image with the selected kernel
switch kernel
    case 'uniform'
%            for z_i=1:size(img,3)
%         for x_i=(radius+1):(size(img,1)+radius)
%             for y_i=(radius+1):(size(img,2)+radius)
%                 sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
%                 img_kernel(x_i-radius, y_i-radius, z_i) = sum(1/2*(abs((sub_img(radius+1, radius+1)-sub_img(indices))) <= alpha));
%             end
%         end
%             end
        % multiply with constant
%        img_kernel = 1/(n*alpha)*img_kernel;
        
        
        k=fspecial('average',radius*2+1);
        k(radius+1,radius+1)=0;
        k(radius+1,radius+1)=-sum(k(:));
        img_kernel=1-abs(imfilter(img,k,'replicate'));

        
    case 'triangle'
        for z_i=1:size(img,3)
            for x_i=(radius+1):(size(img,1)+radius)
                for y_i=(radius+1):(size(img,2)+radius)
                    sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    abs_u = abs((sub_img(radius+1, radius+1)-sub_img(indices))/alpha);
                    img_kernel(x_i-radius, y_i-radius, z_i) = sum( (1-abs_u).*(abs_u <= 1) );
                end
            end
        end
        % multiply with constant
        img_kernel = 1/(n*alpha)*img_kernel;
    case 'epanechnikov'
        for z_i=1:size(img,3)
            for x_i=(radius+1):(size(img,1)+radius)
                for y_i=(radius+1):(size(img,2)+radius)
                    sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    u = (sub_img(radius+1, radius+1)-sub_img(indices))/alpha;
                    img_kernel(x_i-radius, y_i-radius, z_i) = sum((1-u.^2).*(abs(u) <= 1));
                end
            end
        end
        % multiply with constant
        img_kernel = 3/(4*n*alpha)*img_kernel;
    case 'quartic'
        for z_i=1:size(img,3)
            for x_i=(radius+1):(size(img,1)+radius)
                for y_i=(radius+1):(size(img,2)+radius)
                    sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    u = (sub_img(radius+1, radius+1)-sub_img(indices))/alpha;
                    img_kernel(x_i-radius, y_i-radius, z_i) = sum((1-u.^2).^2.*(abs(u) <= 1));
                end
            end
        end
        % multiply with constant
        img_kernel = 15/(16*n*alpha)*img_kernel;
    case 'triweight'
        for z_i=1:size(img,3)
            for x_i=(radius+1):(size(img,1)+radius)
                for y_i=(radius+1):(size(img,2)+radius)
                    sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    u = (sub_img(radius+1, radius+1)-sub_img(indices))/alpha;
                    img_kernel(x_i-radius, y_i-radius, z_i) = sum((1-u.^2).^3.*(abs(u) <= 1));
                end
            end
        end
        % multiply with constant
        img_kernel = 35/(32*n*alpha)*img_kernel;
    case 'gaussian'
        expM1 = exp(-1/2*pad_img.^2*1/alpha^2);
        expM2 = exp(pad_img*1/alpha^2);
         for z_i=1:size(img,3)
            for x_i=(radius+1):(size(img,1)+radius)
                for y_i=(radius+1):(size(img,2)+radius)
                    sub_exp = expM1(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    img_kernel(x_i-radius, y_i-radius, z_i) = sum(expM1(x_i, y_i, z_i)*expM2(x_i, y_i, z_i).^(sub_img(indices)).*sub_exp(indices));
                end
            end
         end
        % multiply with constant
        img_kernel = 1/(n*alpha*sqrt(2*pi))*img_kernel;


        %k=fspecial('average',radius*2+1);
%         k=fspecial('gaussian', radius*2+1, sigma);
%         k(radius+1,radius+1)=0;
%         k(radius+1,radius+1)=-sum(k(:));
%         img_kernel=1-abs(imfilter(img,k,'replicate'));

    case 'cosine'
        for z_i=1:size(img,3)
             for x_i=(radius+1):(size(img,1)+radius)
                for y_i=(radius+1):(size(img,2)+radius)
                    sub_img = pad_img(x_i-radius:x_i+radius, y_i-radius:y_i+radius, z_i);
                    u = (sub_img(radius+1, radius+1)-sub_img(indices))/alpha;
                    img_kernel(x_i-radius, y_i-radius, z_i) = sum(cos(pi/2*u).*(abs(u) <= 1));
                end
             end
        end
        % multiply with constant
        img_kernel = pi/(4*n*alpha)*img_kernel;
    otherwise
        error 'Unknown kernel.'
end

meanI = conv2(img_kernel, ones(7)/49,'same');


% Otsu's tresholding
 %thre = graythresh(img_kernel);
 %img_segm = im2bw(img_kernel,thre);


% figure
% subplot(2,2,1)
% imshow(img_kernel,[])
% 
% subplot(2,2,2)
% imshow(Img2,[])
% 
% subplot(2,2,3)
% % atc IPTG arab case-3
is1 = imdilate( img_kernel<meanI*thss, [0 1 0; 1 1 1; 0 1 0] ) & ( img_kernel<meanI*0.90 );
% case 5
%is1 = imdilate( img_kernel<meanI*0.55, [0 1 0; 1 1 1; 0 1 0] ) & ( img_kernel<meanI*0.90 );
% imshow(is1,[])
% 
% subplot(2,2,4)
% imshow(Img22,[])
% % 
% linkaxes;
is1 = imclose(is1 , ones(5));
img_segm = is1;%imdilate( img_kernel<meanI*0.75, [0 1 0; 1 1 1; 0 1 0] ) & ( img_kernel<meanI*0.90 );
%  asdasda