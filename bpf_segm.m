function img_segm = bpf_segm(img, Ws1, Wp1, Wp2, Ws2,lpf)
% This Matlab function is part of the code package released as a
% supplementary material for the article: 
%
% "Evaluation of methods for detection of fluorescence labeled 
% subcellular objects in microscope images" by P. Ruusuvuori et al. 
%
% We kindly request you to acknowledge the authors properly 
% (citation or request for permission from the authors) when using this
% function.
%
% Website: http://www.cs.tut.fi/sgn/csb/subcell
%
%
%BPF_SEGM    Band pass filtering segmentation algorithm
%
%   usage: img_segm = bfp_segm(img, radius, alpha)
%
%   where:
%       img         3D or 2D array which contains the original image
%       Ws1         first band stop edge (0 < Ws1 < 1)  
%       Wp1         first band pass edge (0 < Ws1 < Wp1 < 1)
%       Wp2         second band pass edge (0 < Ws1 < Wp1 < Wp2 < 1)
%       Ws2         second band stop edge (0 < Ws1 < Wp1 < Wp2 < Ws2 < 1)
%       img_segm    3D or 2D array which contains the image after the
%                   segmentation
%
%   Please cite article "Ruusuvuori et al.: Evaluation of methods for
%   detection of fluorescence labeled subcellular objects in microscope
%   images" when using this function.
%
%   Author: Tarmo �ij� <tarmo.aijo@tut.fi>

% size of the image
img_size = size(img);

img = double(img);

% image should be 2D or 3D
if (length(img_size) ~= 2 && length(img_size) ~= 3) || nargin == 0
    error '2D or 3D image is required.'
end

% default band edges are 
if nargin < 5
    Ws1 = 0.05;
    Wp1 = 0.5;
    Wp2 = 0.6;
    Ws2 = 0.75;
end
if nargin < 6
    % perform low-pass filtering 1 = yes, 0 = no
    lpf = 1;
end
% low-pass filtering
if lpf ~= 0
    if lpf == 1
    lpfl = 5;
    elseif lpf > 1
        lpfl = lpf;
    else
        error('Illegal low-pass filter length!')
    end
    img = padarray(img,[lpfl lpfl],'both','replicate');
    img = filter2(ones(lpfl)/lpfl^2,img,'same');
    img = img(lpfl+1:end-lpfl,lpfl+1:end-lpfl);
end

% band pass filter
b = firpm(6,[0 Ws1 Wp1 Wp2 Ws2 1],[0 0 1 1 0 0]);
% 1D -> 2D
h = ftrans2(b);

img_filt = zeros(img_size);

for z_i=1:size(img, 3)
    img_filt(:,:,z_i) = imfilter(img(:,:,z_i), h, 'replicate');
end

% Otsu's tresholding
img_segm = img_filt>graythresh(img_filt);