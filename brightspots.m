function [vesmask,CF,colored] = brightspots(C,th_sensitivity,visualize,lpf)
% This Matlab function is part of the code package released as a
% supplementary material for the article: 
%
% "Evaluation of methods for detection of fluorescence labeled 
% subcellular objects in microscope images" by P. Ruusuvuori et al. 
%
% We kindly request you to acknowledge the authors properly 
% (citation or request for permission from the authors) when using this
% function.
%
% Website: http://www.cs.tut.fi/sgn/csb/subcell
%
%
% Function for detecting bright spots in image.
%
% IN:   C                   Image
%       th_sensitivity      Threshold for detection sensitivity
%       visualize           Option for displaying the detection result
%       lpf                 Low pass filtering
%
%   Please cite article "Ruusuvuori et al.: Evaluation of methods for
%   detection of fluorescence labeled subcellular objects in microscope
%   images" when using this function.
%

%  PR
%% scaling to mean 0.5
C = double(C)/max(double(C(:)));
C = C + (0.5 - mean(C(:)));

%% check inputs
if nargin < 4
    lpf = 0;
end
if nargin < 3
    visualize = 0;
end
if nargin < 2
    th_sensitivity = 1.6;
end
%% low-pass filtering
if lpf == 1
    lpfl = 3;
    C = padarray(C,[lpfl lpfl],'both','replicate');
    C = filter2(ones(lpfl)/lpfl^2,C,'same');
    C = C(lpfl+1:end-lpfl,lpfl+1:end-lpfl);
end
%%
win = 4; %3
fout = ones(2*win+1);
step = 3; %2
fout(win+1-step:win+1+step,win+1-step:win+1+step) = 0;
% these out-->th=1.20
fout(win+1-step,win+1-step) = 1;
fout(win+1-step,win+1+step) = 1;
fout(win+1+step,win+1-step) = 1;
fout(win+1+step,win+1+step) = 1;
fin = ones(2*win+1);
fin = fin - fout;
fout = fout/(sum(fout(:)));
fin = fin/(sum(fin(:)));

fout_res  = conv2(double(C),fout);
fin_res  = conv2(double(C),fin);
CF = fin_res./fout_res;
CF = CF(win+1:end-win,win+1:end-win);
%th = 0.65  % orig
%th = 1.17;  % w/o extra ones in fout
th = median(CF(:)) + th_sensitivity*std(CF(:));
vesmask = (CF>th);
vesmask(1:win,:) = 0; % use only valid area
vesmask(:,1:win) = 0; % use only valid area
vesmask(end-win:end,:) = 0; % use only valid area
vesmask(:,end-win:end) = 0; % use only valid area

% visualization
colored(:,:,1) = C;
colored(vesmask==1) = max(C(:));
colored(:,:,2) = C;
colored(:,:,3) = C;
colored = double(colored);
colored = colored - min(colored(:));
colored = colored/max(colored(:));
if visualize == 1
    figure, imshow(colored,[])
end