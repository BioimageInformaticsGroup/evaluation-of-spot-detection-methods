function out = morphosegm(S,maxd,lpf)
% This Matlab function is part of the code package released as a
% supplementary material for the article: 
%
% "Evaluation of methods for detection of fluorescence labeled 
% subcellular objects in microscope images" by P. Ruusuvuori et al. 
%
% We kindly request you to acknowledge the authors properly 
% (citation or request for permission from the authors) when using this
% function.
%
% Website: http://www.cs.tut.fi/sgn/csb/subcell
%
%   Please cite article "Ruusuvuori et al.: Evaluation of methods for
%   detection of fluorescence labeled subcellular objects in microscope
%   images" when using this function.
%
%
% Segmentation of bright spots with moprhologigal filtering. Algorithm is
% based on article:
% Prodanov D, Heeroma J, Marani E: Automatic morphometry of synaptic boutons
% of cultured cells using granulometric analysis of digital images. 
% J Neurosci Methods 2006, 151(2):168{177.
%
% IN:   S         input image
%       maxd      maximum disk size for morphological opening (default = 30)
%       lpf       option for low pass filtering (default = 1)
% OUT:  bin       binary result
%

%PR
%% low-pass filtering
if nargin < 3
    lpf = 1;
end
if nargin < 2
    maxd = 30;
end
if lpf == 1
    lpfl = 3;
    S = padarray(S,[lpfl lpfl],'both','replicate');
    S = filter2(ones(lpfl)/lpfl^2,S,'same');
    S = S(lpfl+1:end-lpfl,lpfl+1:end-lpfl);
end
%%
tic
d = 0:1:maxd;
G = zeros(length(d)-1,1);
% calculate initial value for opened S 
% and corresponding parameters hSp and VSp
Spreviousd = imopen(S,strel('disk',d(1)));
[hSp] = hist(double(Spreviousd(:)),0.5:1:255.5);
VSp = sum(hSp.*(0:255));
[hS] = hist(double(S(:)),0.5:1:255.5);  % orig. image histogram
VS = sum(hS.*(0:255));
for ind = 2:length(d)
    Scurrentd = imopen(S,strel('disk',d(ind)));
    [hSc] = hist(double(Scurrentd(:)),0.5:1:255.5);
    VSc = sum(hSc.*(0:255));
    G(d(ind)) = (VSp-VSc)/VS;
    % set "previous" value for the next round
    VSp = VSc;
end
toc
[sortG,sortind] = sort(G,'descend');
dvalues = sortind(1:2);
Ilow = imopen(S,strel('disk',min(dvalues)));
Ihigh = imopen(S,strel('disk',max(dvalues)));
D = Ilow - Ihigh;
DC = kmeans(double(D(:)),2,'Start','uniform');
DC = reshape(DC,size(D));
SMax = S(DC==max(DC(:)));
SMin = S(DC==min(DC(:)));
if mean(SMax) > mean(SMin)
    SM = SMax;
else
    SM = SMin;
    % flip the DC in order to perform masking correctly
    DC = 1-DC;
end
AF = 0.8;
th = 255;
ratio_th = 0;
while ratio_th < AF
    th = th - 1;
    ratio_th = sum(SM>th)/numel(SM);
end

out = (double(S).*(DC==max(DC(:))))>th;
toc

end
