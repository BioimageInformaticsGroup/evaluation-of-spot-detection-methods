function out = atrouswave(I,J,ld,lpf)
% This Matlab function is part of the code package released as a
% supplementary material for the article: 
%
% "Evaluation of methods for detection of fluorescence labeled 
% subcellular objects in microscope images" by P. Ruusuvuori et al. 
%
% We kindly request you to acknowledge the authors properly 
% (citation or request for permission from the authors) when using this
% function.
%
% Website: http://www.cs.tut.fi/sgn/csb/subcell
%
%
% The � trous wavelet segmentation algorithm is based on article:
% Olivo-Marin JC: Extraction of spots in biological images using 
% multiscale products. Pattern Recogn 2002, 35:1989{1996.
%
% IN:
%       I - Image
%       J - Scale (number of decomposition levels)
%       ld - Detection level
%
% Example: out = atrouswave(I);
%
%   Please cite article "Ruusuvuori et al.: Evaluation of methods for
%   detection of fluorescence labeled subcellular objects in microscope
%   images" when using this function.


% PR
if nargin < 4
    % perform low-pass filtering 1 = yes, 0 = no
    lpf = 1;
end
if nargin < 3
    % detection level
    ld = 1;
end
if nargin < 2
    % scale
    J = 3;
end
%% low-pass filtering
if lpf ~= 0
    if lpf == 1
    lpfl = 5;
    elseif lpf > 1
        lpfl = lpf;
    else
        error('Illegal low-pass filter length!')
    end
    I = padarray(I,[lpfl lpfl],'both','replicate');
    I = filter2(ones(lpfl)/lpfl^2,I,'same');
    I = I(lpfl+1:end-lpfl,lpfl+1:end-lpfl);
end


%% wavelet decomposition
% basic kernel
h = [1 4 6 4 1]/16;
% initialize Ai-1 with the original image
Aip = I;
W = zeros(size(I,1),size(I,2),J);
% decomposition scales
for i = 1:J
    % augmented kernel
    ha = [];
    for ind = 1:length(h)-1
        ha = [ha h(ind) zeros(1,2^(i-1)-1)];
    end
    ha = [ha h(ind+1)];
    Aippad = padarray(Aip,[floor(length(ha)/2) floor(length(ha)/2)],'symmetric');
    Ai = conv2(ha,ha',Aippad,'valid');
    W(:,:,i) = Aip - Ai;
    Aip = Ai;
end

%% detection
k = 3; % 3-sigma process :)
t = zeros(J,1);
for tind = 1:J
    frame = W(:,:,tind);
    t(tind) = k*mad(frame(:),1)/0.67;
    frame(frame<t(tind)) = 0;
    W(:,:,tind) = frame;
end
P = prod(W,3);
out = abs(P)>ld;
%figure
%imshow(out,[])
%% reconstruction
%A0 = Ai + sum(W,3);
%imshow(A0,[])
