% This Matlab function is part of the code package released as a
% supplementary material for the article: 
%
% "Evaluation of methods for detection of fluorescence labeled 
% subcellular objects in microscope images" by P. Ruusuvuori et al. 
%
% We kindly request you to acknowledge the authors properly 
% (citation or request for permission from the authors) when using this
% function.
%
% Website: http://www.cs.tut.fi/sgn/csb/subcell
%
%
% IMPLEMENTATION OF EQUATION (19) OF THE ARTICLE
% Multiple Object Tracking in Molecular Bioimaging by Rao-Blackwellized Marginal Particle Filtering
% I. Smal, E. Meijering, K. Draegestein, N. Galjart, I. Grigoriev, A. Akhmanova, M. E. van Royen, A. B. Houtsmuller, W. Niessen
% Medical Image Analysis, vol. 12, no. 6, December 2008, pp. 764-777

% With correction from 
% Quantitative Comparison of Spot Detection
% Methods in Fluorescence Microscopy
% Ihor Smal et al.
% IEEE TRANSACTIONS ON MEDICAL IMAGING

function [clusterMatrix centreList]= detect_h_dome(I, sigma,h, neib, r,radius, sampleCount, win_Size, sigmaM)
    I =  double(I);
    I = I/ max(I(:));
%     sigma = 3; % FREE PARAMETE
%     h = 0.5;  % FREE PARAMETE
%     neib = 7;  % FREE PARAMETE
%     r = 4;  % FREE PARAMETE
%     radius = 3 ;  % FREE PARAMETE
%     sampleCount = 50000;  % FREE PARAMETE
    gt = graythresh(I);
    H = -1*fspecial('log', win_Size, sigma); 
    filtImage = imfilter(I,H,'replicate'); % operation 1
    filtImage  = filtImage - min(filtImage(:) );%extra
    filtImage  = filtImage/max(filtImage(:)); % extra
    
    hdImage = hdTransform2(filtImage, h, neib); % operation 2
    poweredImage = hdImage .^r;% operation 3
    
    probImage = poweredImage/sum(sum(poweredImage)); % operation 4
    
    samples = drawSample(probImage,sampleCount, 0,0); % sample drawing function
   
    [centreList , count, clusterMatrix ] = meanshift(samples, radius); % meanshift cluster function
     clusterMatrix = rejectparticle(clusterMatrix, samples, count, (sigmaM*sigmaM/r)^2);
%     figure
%     subplot(3,2,1)
%     imshow(I,[]);
%     subplot(3,2,2)
%     imshow(samples,[]);
%     subplot(3,2,3)
%     imshow(clusterMatrix,[]);
%     colormap('jet')
%     subplot(3,2,4)
%     imshow(hdImage,[]);
%     subplot(3,2,5)
%     imshow( filtImage,[]);
%     
%    colormap('jet')
%     linkaxes;
% 
%  figure
%     subplot(2,2,1)
%     imshow(I,[]);
%     title('Input Image')
%     subplot(2,2,2)
%     imshow( filtImage,[]);
%     title('LoG filtered Image')
%     subplot(2,2,3)
%     imshow(hdImage,[]);
%     title('H-dome Image')
%     colormap('jet')
%     linkaxes;
    
 function result = rejectparticle(clusterMatrix, samples, count, th2)    
    [r c] = size(samples);
    th1 = sum(samples(:))/(r*c);
    sampleCount = sum(samples(:));
    cArea = regionprops(clusterMatrix, 'ConvexArea' );
    X = cell(1, count);
    Nc = zeros(1, count);
    for i=1:r
        for j=1:c
            for k=1:1:samples(i,j)
                currentColor = clusterMatrix(i,j);    
                Nc(currentColor) = Nc(currentColor) + 1;
                X{currentColor}(Nc(currentColor),1) = i;
                X{currentColor}(Nc(currentColor),2) = j;
            end
        end
    end    
    clustVarianc = zeros(1, count);
    for i=1:count
        if Nc(i) > 1
             clustVarianc(i) = det( cov( X{i} ));
        end
    end
    for i=1:r
        for j=1:c
            currentColor = clusterMatrix(i,j);
            if  currentColor>0
                %if ( Nc(currentColor) < th1* cArea( currentColor,1).ConvexArea ) || ( clustVarianc(currentColor)>th2 )
                if ( Nc(currentColor)*count < sampleCount)  || ( clustVarianc(currentColor)>th2 )
                    clusterMatrix(i,j) = 0; 
                end
            end
        end
    end    
    result = getcompact(clusterMatrix);
    
  function result = getcompact(clusterMatrix)
      clusterMatrix = clusterMatrix +1;
      u = unique(clusterMatrix);
      flag(u) = 1;
      cumflag = cumsum(flag);
      cumflag =  cumflag-1;
      result =  cumflag(clusterMatrix);
    
    
    
        
        
    
% H-dome transform
% 
function hdImage = hdTransform2(I, h, n)
    disp('Enter into h-d transform');
    smallNumber = 1e-7;
    se = strel(ones(n));
    J = I-h;
    flag = 0;
%      [R C] = size(I);
%    count = 0;
    pI = J*0;
    while flag==0
        tempJ = min( imdilate(J, se), I);
        resud = sum( sum(abs(J-tempJ) ));
        J =  tempJ;
        pI = max(pI,J);
%        count = count +1
        if resud < smallNumber
            flag = 1;
        end
    end

    hdImage = I-pI;
%         figure
%     subplot(2,2,1)
%     imshow(I,[])
%     subplot(2,2,2)
%      imshow(pI,[])
%         subplot(2,2,3)
%      imshow( hdImage,[])
%      colormap('jet')
%      linkaxes;
    disp('Exit from h-d transform');
    
    
%% This function draws n samples from 2 dimensional field
% samples image containing samples sample values contains samples in each
% colum

function [samples sampleValues]  = drawSample(prob,n, offsetY, offsetX)
    sampleValues = zeros(2, n);
    r  = size(prob , 1);
    rowSum = sum(prob,1);
    cumRowSum = cumsum(rowSum);
    colSum = cumsum(prob,1);
    samples = prob*0;
    for i=1:n
        col = selectCol(cumRowSum);
        row = selectRow( colSum(:,col), colSum(r,col)  );
        samples(row, col) = samples(row,col)+1;
        sampleValues(:, i) = [ col+ offsetX ; row + offsetY ];
    end

function col = selectCol(cumRowSum)
     n = rand;
     [r,c]= find(cumRowSum>=n ,1, 'first');
     col = c;
     
function row =selectRow( cumSumValues, scaleValue )
      n = rand;
      n = n*scaleValue;
      [r,c]= find( cumSumValues >= n ,1, 'first');
      row = r;

  
      
      
      
function [centreList , count, clusterMatrix ] = meanshift(I, h)
    disp('Enter into meanshift');
    X = imege2Pixel(I);
    % x - col = 2nd dimension  
    % y - row = 1st dimension
    % X -> d X N
    [d N] = size(X);
   
    [nRow nCol] = size(I);
    
    flagMatrix = int16( ones(nRow, nCol)*(-1));
    clusterMatrix = flagMatrix*0;
    bw = I*0;
    count = 0;
    [rowMat colMat vectLen] = buildMatrices(I,h) ;
    initialObjects = (vectLen==0) ; % initial objects are the objects where meanshift vector length = 0;
    [initialObjects count]= bwlabel(initialObjects, 8);
    [centXs centYs] = getCenter(initialObjects);
    centreList(1:count,:) = [centXs' centYs'];
    pnt = [0 0 ];
    [R C] = size(I);
    pointList = zeros(1,R*C);
    pointCount = 0;
    
     flagMatrix =  flagMatrix .* int16(initialObjects==0) + int16(initialObjects);
    
    for i=1:N
        if  flagMatrix ( X(1,i), X(2,i) )  ==  -1
             cX = X(2,i);
             cY = X(1,i);
             converge = 0; % not converged yet
             flagMatrix( cY, cX)= 0; % set the point under process
             pointCount = 0;
             pointCount= pointCount +1;
             pointList(pointCount) = R*(cX-1)+ cY ;
             while converge==0 %as long as not converged
                cX2 =  colMat(cY,cX);
                cY2 =  rowMat(cY,cX);
                if flagMatrix( cY2, cX2 ) == -1  %if status == not processed
                    flagMatrix( cY2, cX2 )= 0; % KEEP IT IN PROCESSLIST
                    cX = cX2; % SET IT AS NEW CENTRE COLUMN
                    cY = cY2; % SET IT AS NEW CENTRE ROW
                    pointCount= pointCount +1; % increse counter
                    pointList(pointCount) = R*(cX-1)+ cY ; % take the point in count
                else % ELSE OK NOW CONVERGED
                    if flagMatrix( cY2, cX2 ) == 0  % converged with a processed point That means its a cycletic
                        flagMatrix(pointList(1:pointCount)) = (count+1); %= flagMatrix + int16(flagMatrix == 0 )*(count+1); % update the matrix
                        pnt = [cY2 cX2 ]; 
                        converge = 1;
                    else 
                        %flagMatrix = flagMatrix + int16( flagMatrix == 0 )*flagMatrix(cY2,cX2 ); % update the matrix
                        flagMatrix(pointList(1:pointCount)) = flagMatrix(cY2,cX2 );
                        converge = 2;
                    end
                end
             end
%             tic
%             [pnt converge flagMatrix] = process(X(2,i),X(1,i),  flagMatrix, rowMat , colMat, count);
%             toc
            if converge==1
                count = count+1;
                centreList(count,:) = pnt;
                bw(pnt(1),pnt(2) ) = count;
            end
        end
%         if mod(i,100)==0
%             i
%         end
       
    end
    clusterMatrix = flagMatrix.*int16(I>0);
    disp('Exit from meanshift');


%pnt = new centre [ r c ]
%theColor = the set and get color
%flagflagMatrix = matrix contains flags
% state -1= nothing happenned
%        0= under process
%        >0 already been processed
function [pnt converge flagMatrix] = process(cX, cY,  flagMatrix, rowMat , colMat, count) 
  
    pnt = [0 0 ];
    converge = 0; % not converged yet
    flagMatrix( cY, cX)= 0; % set the point under process
    while converge==0 %as long as not converged
        cX2 =  colMat(cY,cX);
        cY2 =  rowMat(cY,cX);
        if flagMatrix( cY2, cX2 ) == -1  %if status == not processed
            flagMatrix( cY2, cX2 )= 0; % KEEP IT IN PROCESSLIST
            cX = cX2; % SET IT AS NEW CENTRE COLUMN
            cY = cY2; % SET IT AS NEW CENTRE ROW
        else % ELSE OK NOW CONVERGED
            
            if flagMatrix( cY2, cX2 ) == 0  % converged with a processed point That means its a cycle
                flagMatrix = flagMatrix + int16(flagMatrix == 0 )*(count+1); % update the matrix
                pnt = [cY2 cX2 ]; 
                converge = 1;
            else 
                flagMatrix = flagMatrix + int16( flagMatrix == 0 )*flagMatrix(cY2,cX2 ); % update the matrix
                converge = 2;
            end
        end
    end
    
% CONFUSING FUNCTION
function kern = computeKernel(h)   
     len  = 7*h; % 7 IS THE NUMBER TO SIGNIFICANT PORTION OF AN SQUARED EXPONENTIAL FUNCTION
%    len = h
    len2 = 2*len+1;
    kern = zeros(len2,len2);
    kern(len+1, len+1) = 1;
    kern = bwdist(kern);
    kern = -(kern.*kern) /(2*h*h) ;
    kern = exp(kern);
%     kern = kern / sum(kern(:)); % kernel normalixation
    
 function [rowMat colMat vectLen] = buildMatrices(I,h) 
    kern = computeKernel(h);
    I = double(I);
    [r c] = size(I);
    rowVec  = (1:r)';
    colVec  = (1:c);
   
    rowMat = repmat(rowVec,1,c);
    colMat = repmat(colVec,r,1);
    rm2 = rowMat;
    cm2 = colMat;
    rowMat = rowMat .*I;
    colMat = colMat .*I;
    rowMat = conv2(rowMat, kern,  'same' );
    colMat = conv2(colMat, kern,  'same' );
    distMat = conv2(I, kern,  'same');
    rowMat = round( rowMat./ distMat);
    colMat = round( colMat./ distMat);
    rm2 = abs(rowMat-rm2);
    cm2 = abs(colMat-cm2);
    vectLen = rm2 + cm2;
    
%     figure
%     subplot(2,3,1)
%     imshow(rm,[])
%     subplot(2,3,2)
%     imshow(cm,[])
%     subplot(2,3,3)
%     imshow(rowMat,[])
%     subplot(2,3,4)
%     imshow(colMat,[])
%     subplot(2,3,5)
%     imshow(distMat,[])
%     subplot(2,3,6)
%     imshow(rm+cm,[])
%     colormap('jet');
%     linkaxes;


function expDist = computeDistance( cX1, cY1, cX2, cY2,h)
    d2 = (cX2-cX1).^2 + (cY2-cY1).^2 ;
    d2 = d2/(h*h);
    dist =  exp(-d2/2)/(2*pi);
    expDist(1,:) = dist;
    expDist(2,:) = dist;
  
 function X = imege2Pixel(I)
    [r c] = size(I);
    len = sum(sum(I));
    count = 0;
    X = zeros(2, len);
    for i=1:r
        for j=1:c
            for k=1:1:I(i,j)
                count = count+1;
                X(1,count) = i;
                X(2,count) = j;
            end
        end
    end
  
    
function [cX cY] = getCenter(I)
    cent = regionprops(I, 'Centroid');
    [count temp] = size(cent);
    cX = zeros(1, count);
    cY = zeros(1, count);
    for i= 1:count
        cn =    cent(i).Centroid  ;
        cY(i) = round(cn(2));
        cX(i) = round(cn(1));
    end   
    
    