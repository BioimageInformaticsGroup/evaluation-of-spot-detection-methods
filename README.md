# Evaluation of methods for detection of fluorescence labeled subcellular objects in microscope images

Supplementary site for implementations of the spot detection methods from article 
Ruusuvuori et al. "Evaluation of methods for detection of fluorescence labeled subcellular objects in microscope images", BMC bioinformatics, 11(1), 1-17.

Please cite the [article](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-11-248) when using the methods.

## Authors and acknowledgment
Original method implementations by Sharif Chowdhury, Tarmo Äijö, Jyrki Selinummi and Pekka Ruusuvuori. All other co-authors of the article are acknowledged.

## License
MIT License

Copyright (c) 2010 Pekka Ruusuvuori, Sharif Chowhury, Tarmo Äijö, Jyrki Selinummi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
